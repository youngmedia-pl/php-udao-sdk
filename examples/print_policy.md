# Print policy
---
This example shows how to print a policy to a pdf file using UDAO API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Print policy.
try {
    $pdf = $udao->printPolicy([
        'policy_id' => '{policy-id}',
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSDKException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
header('Content-Type: application/pdf');
echo $pdf;
```
