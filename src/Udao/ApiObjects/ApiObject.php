<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\ApiObjects;

/**
 * Class ApiObject.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class ApiObject extends Collection
{
    /**
     * Returns the value of the field or default if not set.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getField($name, $default = null)
    {
        return isset($this[$name]) ? $this[$name] : $default;
    }

    /**
     * Returns the class the field is mapped to or null if none.
     *
     * @param string $name
     *
     * @return string|null
     */
    public static function getFieldClass($name)
    {
        return isset(static::$fieldMap[$name]) ? static::$fieldMap[$name] : null;
    }

    /**
     * @var string[]
     */
    protected static $fieldMap = [];
}
