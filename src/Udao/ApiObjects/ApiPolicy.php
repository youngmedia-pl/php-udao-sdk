<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\ApiObjects;

/**
 * Class ApiPolicy.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class ApiPolicy extends ApiObject
{
    /**
     * Returns the ID of the policy.
     *
     * @return string
     */
    public function getPolicyId()
    {
        return $this->getField('policy_id');
    }

    /**
     * Returns the number of the policy.
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->getField('number');
    }

    /**
     * @var string[]
     */
    protected static $fieldMap = [
        'policy_id' => 'string',
        'number'    => 'string',
    ];
}
