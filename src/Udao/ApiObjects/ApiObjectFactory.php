<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\ApiObjects;

use Udao\UdaoResponse;

/**
 * Class ApiObjectFactory.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class ApiObjectFactory
{
    /**
     * Creates object of a given class returned in the API response.
     *
     * @param UdaoResponse $response
     * @param string       $class
     *
     * @return ApiObject|ApiList
     */
    public function create(UdaoResponse $response, $class)
    {
        $data = $response->getDecodedBody();

        return $this->createApiObjectOrApiList($data, $class);
    }

    /**
     * Creates an ApiObject of a given class
     * or an ApiList of ApiObject's of a given class
     * depending on data layout.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiObject|ApiList
     */
    private function createApiObjectOrApiList(array $data, $class)
    {
        if ($this->isApiList($data)) {
            return $this->createApiList($data, $class);
        } else {
            return $this->createApiObject($data, $class);
        }
    }

    /**
     * Returns whether the data is an ApiList.
     *
     * @param array $data
     *
     * @return bool
     */
    private function isApiList(array $data)
    {
        // ApiList is an empty array or a sequential numerical array.
        return $data === [] || array_keys($data) === range(0, count($data) - 1);
    }

    /**
     * Creates an ApiList of ApiObject's of a given class.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiList
     */
    private function createApiList(array $data, $class)
    {
        $items = [];

        foreach ($data as $item) {
            $items[] = $this->createApiObject($item, $class);
        }

        return new ApiList($items);
    }

    /**
     * Creates an ApiObject of a given class.
     *
     * @param array  $data
     * @param string $class
     *
     * @return ApiObject
     */
    private function createApiObject(array $data, $class)
    {
        $fields = [];

        foreach ($data as $name => $field) {
            if (null === $fieldClass = $class::getFieldClass($name)) {
                trigger_error(sprintf(
                    'Udao SDK: Unmapped field "%s" in API object "%s"',
                    $name,
                    $class
                ), E_USER_NOTICE);

                continue;
            }

            if (is_array($field)) {
                $fields[$name] = $this->createApiObjectOrApiList($field, $fieldClass);
            } else {
                $fields[$name] = $this->createPrimitive($field, $fieldClass);
            }
        }

        return new $class($fields);
    }

    /**
     * Creates a primitive of given class.
     *
     * @param mixed  $data
     * @param string $class
     *
     * @return mixed
     */
    private function createPrimitive($data, $class)
    {
        if (class_exists($class)) {
            return new $class($data);
        } else {
            settype($data, $class);

            return $data;
        }
    }
}
