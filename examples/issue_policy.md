# Issue policy
---
This example shows how to issue a policy using Udao API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Issue policy.
try {
    $policy = $udao->issuePolicy([
        'calculation_id' => '{calculation-id}',
        'policy_date'    => '2019-12-31T13:00:00+01:00', // ISO-8601 format
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($policy);
```
