<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\Exceptions;

use Udao\UdaoResponse;

/**
 * Class UdaoResponseException.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class UdaoResponseException extends UdaoSdkException
{
    /**
     * Creates a response exception based on the API response.
     *
     * @param UdaoResponse $response
     *
     * @return UdaoResponseException
     */
    public static function create(UdaoResponse $response)
    {
        $code = $response->getHttpStatusCode();
        $message = $response->getBody();

        return new static($message, $code);
    }
}
