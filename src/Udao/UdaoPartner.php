<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao;

/**
 * Class UdaoPartner.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class UdaoPartner
{
    /**
     * Instantiates a new UdaoPartner entity.
     *
     * @param string $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the Udao partner id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     */
    private $id;
}
