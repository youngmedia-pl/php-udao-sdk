# Make calculation
---
This example shows how to make a calculation using Udao API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Make calculation.
try {
    $calculation = $udao->makeCalculation([
        'event_name'        => 'Some event',
        'event_start_date'  => '2024-12-31T13:00:00+01:00', // ISO-8601 format
        'event_end_date'    => '2024-12-31T14:00:00+01:00', // ISO-8601 format
        'event_country'     => 'PL',
        'event_fee'         => '60.99',
        'event_virtual'     => '1', //NULL
        'insured'           => [
            'first_name'    => 'John',
            'last_name'     => 'Doe',
            'phone'         => '123123123',
            'email'         => 'john@example.com',
            'birth_date'    => '1950-12-31',
            'birth_country' => 'PL',
            'nationality'   => 'PL',
            'zip_code'      => '00-001',
            'city'          => 'City',
            'street'        => 'Street',
            'house_number'  => '2/1',
            'flat_number'   => '3a',
        ],
        'policy_holder'     => [
            'first_name'    => 'John',
            'last_name'     => 'Doe',
            'phone'         => '123123123',
            'email'         => 'john@example.com',
            'birth_date'    => '1950-12-31',
            'birth_country' => 'PL',
            'nationality'   => 'PL',
            'zip_code'      => '00-001',
            'city'          => 'City',
            'street'        => 'Street',
            'house_number'  => '2/1',
            'flat_number'   => '3a',
        ],
        'product_id'        => '{product-id}',
        'agreements'        => [
            // Zgody dotyczące wszystkich partnerów.
            'GDPR', // (wymagane) zgoda na przetwarzanie danych osobowych (General Data Protection Regulation)
            'DCAR', // (wymagane) zgoda na cyfrowy nośnik informacji (Digital Carrier)

            // Zgody dotyczące wszystkich partnerów z wyjątkiem Runmageddon.
            'GTOS', // (wymagane) zgoda na ogólne warunki ubezpieczenia (General Terms of Service)
            'FFPP', // (wymagane) zgoda na adekwatność produktu (Fitness For Particular Purpose)
            'ECOM', //            zgoda na marketing drogą elektroniczną (Email Commercials)

            // Zgody dotyczące tylko partnera Runmageddon.
            'STOS', // (wymagane) zgoda na szczególne warunki ubezpieczenia (Special Terms Of Service)
            'XPAY', // (wymagane) zgoda na finansowanie składki w pakiecie dodatkowym (Extra Pay)
        ],
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($calculation);
```
