<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\ApiObjects;

use DateTime;
use DateTimeInterface;

/**
 * Class ApiCalculation.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class ApiCalculation extends ApiObject
{
    /**
     * Returns the ID of the calculation.
     *
     * @return string
     */
    public function getCalculationId()
    {
        return $this->getField('calculation_id');
    }

    /**
     * Returns the date of the calculation.
     *
     * @return DateTimeInterface
     */
    public function getDate()
    {
        return $this->getField('date');
    }

    /**
     * Returns the date due to the calculation is valid.
     *
     * @return DateTimeInterface
     */
    public function getDueDate()
    {
        return $this->getField('due_date');
    }

    /**
     * Returns the premium to be paid for the calculation.
     *
     * @return float
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * @var string[]
     */
    protected static $fieldMap = [
        'calculation_id' => 'string',
        'date'           => DateTime::class,
        'due_date'       => DateTime::class,
        'premium'        => 'float',
    ];
}
