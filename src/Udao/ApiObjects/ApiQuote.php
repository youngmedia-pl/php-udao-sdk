<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao\ApiObjects;

/**
 * Class ApiQuote.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class ApiQuote extends ApiObject
{
    /**
     * Returns the insurance product offered in the quote.
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->getField('product_id');
    }

    /**
     * Returns the name of the quote.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getField('name');
    }

    /**
     * Returns the description of the quote.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getField('description');
    }

    /**
     * Returns the premium to be paid for the quote.
     *
     * @return float
     */
    public function getPremium()
    {
        return $this->getField('premium');
    }

    /**
     * Returns the risks defined in the quote.
     *
     * @return ApiList|ApiRisk[]
     */
    public function getRisks()
    {
        return $this->getField('risks');
    }

    /**
     * Returns whether a risk is defined in this quote.
     *
     * @param string $code
     *
     * @return bool
     */
    public function hasRisk($code)
    {
        $codes = array_column($this->getRisks()->asArray(), 'code');

        return in_array($code, $codes);
    }

    /**
     * @var string[]
     */
    protected static $fieldMap = [
        'product_id'  => 'string',
        'name'        => 'string',
        'description' => 'string',
        'premium'     => 'float',
        'risks'       => ApiRisk::class,
    ];
}
