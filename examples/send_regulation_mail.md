# Send regulation mail
---
This example shows how to send regulation mail using Udao API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Send regulation mail.
try {
    $success = $udao->sendRegulationMail([
        'person_email' => 'john@example.com',
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo($e->getMessage());
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo($e->getMessage());
    exit;
}

// Prints "true" on success.
var_dump($success);
```
