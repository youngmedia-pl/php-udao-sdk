<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao;

use Udao\ApiObjects\ApiCalculation;
use Udao\ApiObjects\ApiList;
use Udao\ApiObjects\ApiPolicy;
use Udao\ApiObjects\ApiQuote;
use Udao\Exceptions\UdaoSdkException;

/**
 * Main class.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class Udao
{
    /**
     * @var string Version number of the UDAO SDK for PHP.
     */
    const VERSION = '0.5.1';

    /**
     * @var string Default environment to use.
     */
    const DEFAULT_ENVIRONMENT = 'prod';

    /**
     * @var int Default request timeout in seconds.
     */
    const DEFAULT_REQUEST_TIMEOUT = 10;

    /**
     * Instantiates a new Udao main object.
     *
     * @param array $config
     *
     * @throws UdaoSdkException
     */
    public function __construct(array $config = [])
    {
        $config += [
            'partner_id'      => null,
            'environment'     => static::DEFAULT_ENVIRONMENT,
            'request_timeout' => static::DEFAULT_REQUEST_TIMEOUT,
        ];

        if (!$config['partner_id']) {
            throw new UdaoSdkException('Required "partner_id" key not supplied in config.');
        }

        $this->partner = new UdaoPartner($config['partner_id']);
        $this->client = new UdaoClient();
        $this->environment = $config['environment'];
        $this->requestTimeout = $config['request_timeout'];
    }

    /**
     * Tests connection with the API and returns "echo" on success.
     *
     * @return string
     */
    public function echoApi()
    {
        $response = $this->send('GET', '/echo');

        return $response->getBody();
    }

    /**
     * Gets quotes from the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return ApiList|ApiQuote[]
     */
    public function getQuotes(array $params)
    {
        $response = $this->send('GET', '/insurance/quotes', $params);

        return $response->getQuotes();
    }

    /**
     * Sends regulation mail via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return bool
     */
    public function sendRegulationMail(array $params)
    {
        $response = $this->send('POST', '/insurance/regulation-mail', $params);

        return $response->getHttpStatusCode() == 200;
    }

    /**
     * Makes calculation via the API.
     *
     * @param array $params
     *
     * @return ApiCalculation
     */
    public function makeCalculation(array $params)
    {
        $response = $this->send('POST', '/insurance/calculations', $params);

        return $response->getCalculation();
    }

    /**
     * Recalculates via the API.
     *
     * @param array $params
     *
     * @return ApiCalculation
     */
    public function recalculate(array $params)
    {
        $response = $this->send('PUT', '/insurance/calculations', $params);

        return $response->getCalculation();
    }

    /**
     * Issues policy via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return ApiPolicy
     */
    public function issuePolicy(array $params)
    {
        $response = $this->send('POST', '/insurance/policies', $params);

        return $response->getPolicy();
    }

    /**
     * Prints policy to PDF via the API.
     *
     * See SDK examples for description of parameters.
     *
     * @param array $params
     *
     * @return string
     */
    public function printPolicy(array $params)
    {
        $request = $this->createRequest('GET', '/insurance/policies', $params);

        $request->setHeaders([
            'Accept' => 'application/pdf',
        ]);

        $response = $this->client->sendRequest($request);

        return $response->getBody();
    }

    /**
     * Sends a request to the API and returns the response.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $params
     *
     * @return UdaoResponse
     */
    public function send($method, $endpoint, array $params = [])
    {
        $request = $this->createRequest($method, $endpoint, $params);

        return $this->client->sendRequest($request);
    }

    /**
     * Creates a request.
     *
     * @param string $method
     * @param string $endpoint
     * @param array  $params
     *
     * @return UdaoRequest
     */
    private function createRequest($method, $endpoint, array $params = [])
    {
        return new UdaoRequest(
            $this->partner,
            $method,
            $endpoint,
            $params,
            $this->environment,
            $this->requestTimeout
        );
    }

    /**
     * @var UdaoPartner
     */
    private $partner;

    /**
     * @var UdaoClient
     */
    private $client;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var int
     */
    private $requestTimeout;
}
