<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao;

/**
 * Class UdaoRequest.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class UdaoRequest
{
    /**
     * Instantiates a new UdaoRequest entity.
     *
     * @param UdaoPartner $partner
     * @param string      $method
     * @param string      $endpoint
     * @param array       $params
     * @param string      $environment
     * @param int         $timeout
     */
    public function __construct(UdaoPartner $partner, $method, $endpoint, array $params = [], $environment, $timeout)
    {
        $this->partner = $partner;
        $this->method = $method;
        $this->endpoint = $endpoint;
        $this->params = $params;
        $this->environment = $environment;
        $this->timeout = $timeout;
        $this->headers = [];
    }

    /**
     * Returns the URL of the request.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getBaseUrl() . $this->getRelativeUrl();
    }

    /**
     * Returns the HTTP method of the request.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Returns the headers of the request.
     *
     * @return array
     */
    public function getHeaders()
    {
        $defaultHeaders = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
            'X-Partner-Id'  => $this->partner->getId(),
        ];

        return $this->headers + $defaultHeaders;
    }

    /**
     * Sets headers of the request.
     *
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers + $this->headers;
    }

    /**
     * Returns the body of the request.
     *
     * @return string
     */
    public function getBody()
    {
        return json_encode($this->getBodyParams());
    }

    /**
     * Returns the timeout of the request.
     *
     * @return int
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * Returns the base URL of the request.
     *
     * @return string
     */
    private function getBaseUrl()
    {
        return $this->environment === 'prod' ? UdaoClient::BASE_URL : UdaoClient::BASE_URL_DEV;
    }

    /**
     * Returns the relative URL of the request.
     *
     * @return string
     */
    private function getRelativeUrl()
    {
        return $this->endpoint . '?' . http_build_query($this->getUrlParams(), '', '&');
    }

    /**
     * Returns the parameters to be sent in the URL of the request.
     *
     * @return array
     */
    private function getUrlParams()
    {
        return $this->method === 'GET' ? $this->params : [];
    }

    /**
     * Returns the parameters to be sent in the body of the request.
     *
     * @return array
     */
    private function getBodyParams()
    {
        return $this->method !== 'GET' ? $this->params : [];
    }

    /**
     * @var UdaoPartner
     */
    private $partner;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $environment;

    /**
     * @var int
     */
    private $timeout;

    /**
     * @var array
     */
    private $headers;
}
