# Get quotes
---
This example shows how to get quotes using Udao API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Get quotes.
try {
    $quotes = $udao->getQuotes([
        'event_date'        => '2020-12-31',
        'event_fee'         => '60.99',
        'person_birth_date' => '1950-12-31',
        'person_email'      => 'john@example.com',
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($quotes);
```
