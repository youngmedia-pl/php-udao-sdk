# Create checkboxes
---
This example shows how to create checkboxes for the insurance form.

## Example
---

```php
<?php

// Get quotes.
// $quotes = ...

// Create checkboxes.
/** @var ((ApiQuote[])[])[] */
$checkboxes = [];

foreach ($quotes as $quote) {
    $hasAccident = $quote->hasRisk('ACC');
    $hasResignation = $quote->hasRisk('RES');
    $hasRehablitation = $quote->hasRisk('REH');

    $checkboxes[$hasAccident][$hasResignation][$hasRehablitation] = $quote;
}

// Print checkboxes prices.
echo $checkboxes[1][0][0]->getPremium() . PHP_EOL; // accident checkbox price
echo $checkboxes[0][1][0]->getPremium() . PHP_EOL; // resignation checkbox price
echo $checkboxes[0][0][1]->getPremium() . PHP_EOL; // rehabilitation checkbox price

// Print selected quote,
// eg. user selected accident and resignation checkbox.
var_dump($checkboxes[1][1][0]);
```
