<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao;

use Udao\Exceptions\UdaoResponseException;
use Udao\Exceptions\UdaoSdkException;

/**
 * Class UdaoClient.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class UdaoClient
{
    /**
     * @var string Base API URL.
     */
    const BASE_URL = 'https://api.udao.pl';

    /**
     * @var string Base development API URL.
     */
    const BASE_URL_DEV = 'http://dev.api.udao.pl';

    /**
     * Sends a request and returns the response.
     *
     * @param UdaoRequest $request
     *
     * @return UdaoResponse
     *
     * @throws UdaoResponseException
     */
    public function sendRequest(UdaoRequest $request)
    {
        $this->openConnection($request);

        $httpResponse = $this->executeConnection();

        $this->closeConnection();

        $response = UdaoResponse::create($httpResponse);

        if ($response->isError()) {
            throw $response->getThrownException();
        }

        return $response;
    }

    /**
     * Opens a new curl connection.
     *
     * @param UdaoRequest $request
     */
    private function openConnection(UdaoRequest $request)
    {
        $options = [
            CURLOPT_CAINFO         => __DIR__ . '/BaltimoreCyberTrustRoot.pem',
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_CUSTOMREQUEST  => $request->getMethod(),
            CURLOPT_HEADER         => true,
            CURLOPT_HTTPHEADER     => $this->compileHeaders($request->getHeaders()),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_TIMEOUT        => $request->getTimeout(),
            CURLOPT_URL            => $request->getUrl(),
        ];

        if ($request->getMethod() !== 'GET') {
            $options[CURLOPT_POSTFIELDS] = $request->getBody();
        }

        $this->curl = curl_init();
        curl_setopt_array($this->curl, $options);
    }

    /**
     * Executes the curl connection and returns the http response.
     *
     * @return string
     *
     * @throws UdaoSdkException
     */
    private function executeConnection()
    {
        $httpResponse = curl_exec($this->curl);
        $errorCode = curl_errno($this->curl);
        $errorMessage = curl_error($this->curl);

        if ($errorCode) {
            throw new UdaoSdkException($errorMessage, $errorCode);
        }

        return $httpResponse;
    }

    /**
     * Closes the curl connection.
     */
    private function closeConnection()
    {
        curl_close($this->curl);
    }

    /**
     * Compiles headers into a curl-friendly format.
     *
     * @param array $headers
     *
     * @return array
     */
    private function compileHeaders(array $headers)
    {
        $compiledHeaders = [];

        foreach ($headers as $key => $value) {
            $compiledHeaders[] = $key . ': ' . $value;
        }

        return $compiledHeaders;
    }

    /**
     * @var resource
     */
    private $curl;
}
