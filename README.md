# Udao SDK for PHP
---
This repository contains the PHP SDK that allows you to access the [UDAO](https://udao.pl/) API from your project.

## Installation
---
The Udao PHP SDK can be installed with [Composer](https://getcomposer.org/). Run this command:
```
composer require youngmedia-pl/udao-sdk
```

## Usage
---
Simple example of getting the quotes.

```php
<?php

require_once __DIR__ . '/vendor/autoload.php'; // change path as needed

$udao = new Udao\Udao([
    'partner_id'  => '{partner-id}', // the private partner id
    'environment' => 'prod',         // (default) change to 'dev' for development environment
]);

// Get quotes.
try {
    $quotes = $udao->getQuotes([
        'event_date'        => '2020-12-31',
        'event_fee'         => '60.99',
        'person_birth_date' => '1950-12-31',
        'person_email'      => 'john@example.com',
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($quotes);
```
