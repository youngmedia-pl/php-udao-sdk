# Echo API
---
This example shows how to test connection with the Udao API using SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Echo API.
try {
    $echo = $udao->echoApi();
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSdkException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Prints "echo" on success.
var_dump($echo);
```
