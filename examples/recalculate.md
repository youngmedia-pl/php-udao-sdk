# recalculate
---
This example shows how to recalculate using UDAO API and SDK for PHP.

## Example
---

```php
<?php

$udao = new Udao\Udao([
    'partner_id' => '{partner-id}',
]);

// Recalculate.
try {
    $pdf = $udao->recalculate([
        'calculation_id' => '{calculation-id}',
    ]);
} catch (Udao\Exceptions\UdaoResponseException $e) {
    // When API returns an error.
    echo 'API returned an error: ' . $e->getMessage();
    exit;
} catch (Udao\Exceptions\UdaoSDKException $e) {
    // Other issues.
    echo 'SDK returned an error: ' . $e->getMessage();
    exit;
}

// Print details.
var_dump($calculation);
```
