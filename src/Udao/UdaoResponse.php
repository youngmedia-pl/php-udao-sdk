<?php

/**
 * This file is part of the Udao SDK for PHP.
 *
 * Copyright (c) 2020 Youngmedia.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Udao;

use Udao\ApiObjects\ApiCalculation;
use Udao\ApiObjects\ApiList;
use Udao\ApiObjects\ApiObjectFactory;
use Udao\ApiObjects\ApiPolicy;
use Udao\ApiObjects\ApiQuote;
use Udao\Exceptions\UdaoResponseException;

/**
 * Class UdaoResponse.
 *
 * @author Łukasz Proksa <lukasz.proksa@youngmedia.pl>
 *
 * @package Udao
 */
class UdaoResponse
{
    /**
     * Creates response from http response.
     *
     * @param string $httpResponse
     *
     * @return UdaoResponse
     */
    public static function create($httpResponse)
    {
        // Separate response headers and body.
        $parts = explode("\r\n\r\n", $httpResponse);

        // Get response body.
        $body = array_pop($parts);

        // Get response headers.
        // There will be multiple headers, if a 301 was followed, etc. We just want the last one.
        $httpHeaders = array_pop($parts);

        // Parse response headers.
        $headers = [];
        $httpStatusCode = null;

        foreach (explode("\r\n", $httpHeaders) as $httpHeader) {
            if (strpos($httpHeader, ': ') === false) {
                $detectHttpStatusCode = '|HTTP/\d\.\d\s+(\d+)\s+.*|';
                preg_match($detectHttpStatusCode, $httpHeader, $match);
                $httpStatusCode = (int) $match[1];
            } else {
                list($key, $value) = explode(': ', $httpHeader, 2);
                $headers[$key] = $value;
            }
        }

        return new static($headers, $body, $httpStatusCode);
    }

    /**
     * Instantiates a new UdaoResponse entity.
     *
     * @param array  $headers
     * @param string $body
     * @param int    $httpStatusCode
     */
    public function __construct(array $headers, $body, $httpStatusCode)
    {
        $this->headers = $headers;
        $this->body = $body;
        $this->httpStatusCode = $httpStatusCode;

        $this->decodeBodyAndError();
    }

    /**
     * Returns the headers of the response.
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Returns the body of the response.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Return the HTTP status code of the response.
     *
     * @return int
     */
    public function getHttpStatusCode()
    {
        return $this->httpStatusCode;
    }

    /**
     * Returns the decoded body of the response.
     *
     * @return array|null
     */
    public function getDecodedBody()
    {
        return $this->decodedBody;
    }

    /**
     * Returns whether the API returned an error.
     *
     * @return bool
     */
    public function isError()
    {
        $successHttpStatusCodes = [200, 201];

        return !in_array($this->httpStatusCode, $successHttpStatusCodes);
    }

    /**
     * Returns the exception thrown by this request.
     *
     * @return UdaoResponseException|null
     */
    public function getThrownException()
    {
        return $this->thrownException;
    }

    /**
     * Convenience method for creating an ApiList of ApiQuote objects.
     *
     * @return ApiList|ApiQuote[]
     */
    public function getQuotes()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiQuote::class);
    }

    /**
     * Convenience method for creating an ApiCalculation object.
     *
     * @return ApiCalculation
     */
    public function getCalculation()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiCalculation::class);
    }

    /**
     * Convenience method for creating an ApiPolicy object.
     *
     * @return ApiPolicy
     */
    public function getPolicy()
    {
        $factory = new ApiObjectFactory();

        return $factory->create($this, ApiPolicy::class);
    }

    /**
     * Decodes body and error of the API response.
     */
    private function decodeBodyAndError()
    {
        $this->decodeBody();

        if ($this->isError()) {
            $this->createException();
        }
    }

    /**
     * Decodes body of the API response.
     */
    private function decodeBody()
    {
        $this->decodedBody = json_decode($this->body, true);
    }

    /**
     * Creates an exception to be thrown later.
     */
    private function createException()
    {
        $this->thrownException = UdaoResponseException::create($this);
    }

    /**
     * @var array
     */
    private $headers;

    /**
     * @var string
     */
    private $body;

    /**
     * @var int
     */
    private $httpStatusCode;

    /**
     * @var array|null
     */
    private $decodedBody;

    /**
     * @var UdaoResponseException|null
     */
    private $thrownException;
}
